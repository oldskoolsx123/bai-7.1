package com.example.bai71.Entity;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class SingletonArrayList {

    private static SingletonArrayList instance;
    private ArrayList<String> arrayList;

    private SingletonArrayList() {
        arrayList = new ArrayList<>();
    }


    public static SingletonArrayList getInstance(){
        if(instance == null){
            instance = new SingletonArrayList();
        }
        return instance;
    }

    public void add(String token){
        arrayList.add(token);
    }

    public void RemoveTheOutdatedToken()  {
        if(!arrayList.isEmpty()){
            arrayList.remove(0);
            System.out.println("Token Has been Deleted");
        }
    }
}
