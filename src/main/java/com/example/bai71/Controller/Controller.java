package com.example.bai71.Controller;

import com.example.bai71.Service.MainInterface;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("login")
public class Controller {

    private final MainInterface i;

    public Controller(@Qualifier("mainImplement") MainInterface i) {
        this.i = i;
    }

    @PostMapping("")
    ResponseEntity login(@RequestParam String userName, @RequestParam String passWord){
        return new ResponseEntity<>(i.login(userName,passWord), HttpStatus.OK);
    }
}
