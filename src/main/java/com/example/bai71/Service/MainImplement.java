package com.example.bai71.Service;

import com.example.bai71.Entity.SingletonArrayList;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

@Component
public class MainImplement implements MainInterface{
    @Override
    public String login(String userName, String passWord) {
        try {
            Timer timer = new Timer();
            if(userName.equals("admin") && passWord.equals("123")){
                UUID uuid = UUID.randomUUID();
                SingletonArrayList singletonArrayList = SingletonArrayList.getInstance();
                singletonArrayList.add(uuid.toString());
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        singletonArrayList.RemoveTheOutdatedToken();
                    }
                }, 10 * 60 * 1000);
                return "Thành công";
            }else{
                System.out.println(userName + passWord);
                return "Thất bại";
            }
        } catch (Exception e){
            System.out.println(e);
            return "Thất bại";
        }
    }
}
