package com.example.bai71.Service;

import org.springframework.stereotype.Service;

@Service
public interface MainInterface {
    String login(String userName, String passWord);
}
